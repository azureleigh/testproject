<?php
namespace Tests\Unit;

use Tests\TestCase;
use Mockery;

class MailchimpTest extends TestCase 
{
    public $strMemberEmailAddress = 'dogibok@webaward.online';
    public $strMemberStatus = 'subscribed';

    public function test_mock_createMember() 
    {
        $arrData = [
            'email_address' => $this->strMemberEmailAddress,
            'status' => $this->strMemberStatus
        ];
        $strMethod = "POST";
        $objMock = Mockery::mock('App\Mailchimp');

        $objMock->shouldReceive('createMember')
                ->once()
                ->andReturn('Mocked return: Member created');
        $objMock->createMember($arrData, $strMethod);
    }

    public function test_mock_updateMember() 
    {
        
        $arrData = [
            'status' => 'unsubscribed',
        ];
        $strMemberId = md5($this->strMemberEmailAddress);
        $strMethod = "PATCH";
        $objMock = Mockery::mock('App\Mailchimp');

        $objMock->shouldReceive('updateMember')
                ->once()
                ->withArgs([$arrData, $strMethod, $strMemberId])
                ->andReturn('Mocked return: status updated');
        $objMock->updateMember($arrData, $strMethod, $strMemberId);
    }

    public function test_mock_removeMember() 
    {
        $strMemberId = md5($this->strMemberEmailAddress);
        $strMethod = "DELETE";

        $objMock = Mockery::mock('App\Mailchimp');

        $objMock->shouldReceive('removeMember')
                ->once()
                ->withArgs([$strMethod, $strMemberId])
                ->andReturn('Mocked return: member deleted');
        $objMock->removeMember($strMethod, $strMemberId);
    }


    public function test_createMember() 
    {
        $arrData = [
            'email_address' => $this->strMemberEmailAddress,
            'status' => $this->strMemberStatus
        ];
        $strMemberId = md5($this->strMemberEmailAddress);
        $objRequest = $this->json(
            'POST',
            '/mailchimp/create',
            $arrData
        );

        $objRequest->assertStatus(200);
        $objRequest->assertJson([
            'email_address' => $this->strMemberEmailAddress,
            'status' => $this->strMemberStatus
        ]);

        $objResponse = json_decode($objRequest->getContent());

        $this->assertEquals($objResponse->id, $strMemberId);
    }

    public function test_updateMember() 
    {
        $arrData = [
            'status' => 'unsubscribed'
        ];
        $strMemberId = md5($this->strMemberEmailAddress);
        $objRequest = $this->json(
            'PATCH',
            '/mailchimp/patch/' . $strMemberId,
            $arrData
        );

        $objRequest->assertStatus(200);
        $objRequest->assertJson([
            'status' => 'unsubscribed'
        ]);

        $objResponse = json_decode($objRequest->getContent());

        $this->assertEquals($objResponse->id, $strMemberId);
    }

    public function test_removeMember() 
    {
        $strMemberId = md5($this->strMemberEmailAddress);
        $objRequest = $this->delete(
            '/mailchimp/delete/' . $strMemberId,
            ['Content-Type' => 'application/x-www-form-urlencoded']
        );

        $objRequest->assertStatus(200);
    }
}
?>