<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

/***
* Mailchimp route
*/
Route::prefix('mailchimp')->group(function () {
	// GET
	Route::get('/get','MailchimpController@get');
	// Post 
	Route::post('/create','MailchimpController@create');
	// Delete
	Route::delete('/delete/{id}','MailchimpController@delete');
	// Patch
	Route::patch('/patch/{id}','MailchimpController@patch');
});