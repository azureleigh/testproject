<?php
namespace App;

use Validator;

class Mailchimp 
{
	protected $strAppBaseUrl;

	public function __construct()
    {
        $this->strAppBaseUrl = config('services.mailchimp.url');
    }

    /**
    * Get members
    *
    * @param array $arrData mailchimp fields and values
    * @param string $strMethod Accepts method POST
    *
    * @return object
    */
    public function getMembers($strMethod)
    {
        $objResponse = $this->curlRequest($strMethod, "members");
        
        return $objResponse;
    }

    /**
    * Update existing member
    *
    * @param array $arrData mailchimp fields and values
    * @param string $strMethod Accepts method POST
    *
    * @return object
    */
    public function createMember($arrData, $strMethod) 
    {
        $objErrors = $this->validateRequest($arrData, $strMethod);

        if (count($objErrors)) {
        	return $objErrors;
        }

        $objResponse = $this->curlRequest($strMethod, "members", $arrData);
        
        return $objResponse;
    }

    /**
    * Update existing member
    *
    * @param array $arrData mailchimp fields and values
    * @param string $strMethod Accepts method PATCH
    * @param string $strId member id
    *
    * @return object
    */
   
    public function updateMember($arrData, $strMethod, $strId) 
    {
    	$objErrors = $this->validateRequest($arrData, $strMethod);

        if (count($objErrors)) {
        	return $objErrors;
        }

        if (count($arrData) == 0) {
        	throw new \Exception('Please specify a field to update');
        }

        $objResponse = $this->curlRequest($strMethod, "members/" . $strId, $arrData);
        
        return $objResponse;
    }

    /**
    * Remove member from a list 
    * @param string $strMethod Accepts method DELETE
    * @param string $strId member id
    *
    * @return object
    */
    public function removeMember($strMethod, $strId) 
    {
    	$objResponse = $this->curlRequest($strMethod, "members/" . $strId);
        
        return $objResponse;
    }

    /**
    * Validate request parameters
    * 
    * @param array $arrData mailchimp fields and values
    * @param string $strMethod Accepts method PATCH, POST
    *
    * @return void|object
    */
    public function validateRequest($arrData, $strMethod) 
    {
        $arrAllowedStatus = [
            'subscribed',
            'unsubscribed',
            'cleaned',
            'pending'
        ];

        $arrEmailType = [
        	'html',
        	'text'
        ];

        $arrCommonRules = [
        	'email_type' => 'string|in:' . implode(',', $arrEmailType),
        	'merge_fields.FNAME' => 'string',
        	'merge_fields.LNAME' => 'string',
        	'merge_fields.ADDRESS' => 'string',
        	'merge_fields.PHONE' => 'string',
        	'language' => 'string',
        	'vip'=> 'boolean',
        	'stats.avg_open_rate' => 'numeric',
        	'stats.avg_click_rate' => 'numeric',
        	'ip_signup' => 'string',
        	'ip_opt' => 'string',
        	'timestamp_signup' => 'date_format:Y-m-d\TH:i:sO',
        	'timestamp_opt' => 'date_format:Y-m-d\TH:i:sO',
        	'member_rating' => 'integer',
        	'last_changed' => 'date_format:Y-m-d\TH:i:sO',
        	'email_client' => 'string'
        ];

        if ($strMethod == "PATCH") {
            $arrRules = [
                'email_address' => 'email',
                'status' => 'in:' . implode(',', $arrAllowedStatus),
            ];
        } else {
            $arrRules = [
                'email_address' => 'required|email',
                'status' => 'required|in:' . implode(',', $arrAllowedStatus),
            ];
        }

        $objValidator = Validator::make($arrData, array_merge($arrRules, $arrCommonRules));

        if ($objValidator->fails()) {
        	return $objValidator->errors();
        }
    }

    /**
    * Create curl request
    * 
    * @param string $strMethod Accepts curl request types: POST, GET, PATCH, PUT, DELETE
    * @param string $strTarget target module = member
    * @param array member field and values
    * 
    * @return json mailchimp response
    */
    public function curlRequest($strMethod, $strTarget = "", $arrData = array()) 
    {
        $strUrl = !empty($strTarget) ? $this->strAppBaseUrl . $strTarget : $this->strAppBaseUrl;
        $objCurl = curl_init($strUrl);
        $arrHeaders = array(
            'Content-Type: application/json', 
            'Authorization: ' . config('services.mailchimp.login') . ' ' . config('services.mailchimp.key'),
        );

        curl_setopt($objCurl, CURLOPT_HTTPHEADER, $arrHeaders);
        curl_setopt($objCurl, CURLOPT_CUSTOMREQUEST, $strMethod);
        curl_setopt($objCurl, CURLOPT_TIMEOUT, 5);
        curl_setopt($objCurl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($objCurl, CURLOPT_USERAGENT, 'YOUR-USER-AGENT');

        if ($arrData) {
            curl_setopt($objCurl, CURLOPT_POSTFIELDS, json_encode($arrData));
        }

        $objResponse = curl_exec($objCurl);

        curl_close($objCurl);
        
        return $objResponse;
    }
}
?>