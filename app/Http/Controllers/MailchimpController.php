<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mailchimp;

class MailchimpController extends Controller
{
    protected $objMailchimp;

    public function __construct(Mailchimp $objMailchimp)
    {
        $this->objMailchimp = $objMailchimp;
    }

    public function get(Request $objRequest) 
    {
        $objResponse = $this->objMailchimp->getMembers($objRequest->method());

        return $objResponse;
    }

    public function create(Request $objRequest) 
    {
        $objResponse = $this->objMailchimp->createMember($objRequest->all(), $objRequest->method());
       
        return $objResponse;
    }

    public function patch(Request $objRequest, $strId) 
    {
        $objResponse = $this->objMailchimp->updateMember($objRequest->all(), $objRequest->method(), $strId);

        return $objResponse;
    }

    public function delete(Request $objRequest, $strId) 
    {
        $objResponse = $this->objMailchimp->removeMember($objRequest->method(), $strId);
        
        return $objResponse;
    }
}
?>